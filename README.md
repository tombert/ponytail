# ponytail
Store your configuration files as gists

## Spec
* Implementation
The only implementation goal is to use a lightweight language. It needs to be statically linked and easy to distribute. Current ideas are Go and Haskell.

* Saving a new file:
```
[user@dev ~]$ ponytail save ~/.Xdefaults
```

* Updating all files:
```
[user@dev ~]$ ponytail update
```

* Deleting a file:
```
[user@dev ~]$ ponytail delete .Xdefaults
==> Delete file versioned file referenced at ~/.Xdefaults ? (y/N)
```

* ponyfile:
```
{
  "username": "dev",
  "gists": [
    "0a2b69dc4b3359bd22bb",
    "3563f630d2b9daf79861",
    "a1d86a43acf82d960054",
    "c384e522a080e47242c1"
  ]
}
```
